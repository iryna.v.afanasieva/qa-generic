package org.learn.qa.generic;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User {
    private Gender gender;
    private Name name;
    private Location location;
    private String email;
    private Login login;
    @JsonProperty("dob")
    private DateAndAge dateOfBirth;
    @JsonProperty("registered")
    private DateAndAge dateOfRegistration;
    private String phone;
    private String cell;
    private Identifier id;
    private Picture picture;
    private String nat;
    private List<Client> clients;

    public enum Gender {
        MALE("male"),
        FEMALE("female"),
        UNKNOWN("unknown");

        private String value;

        Gender(String value) {
            this.value = value;
        }

        @JsonCreator
        public static Gender getGenderFromValue(final String value) {
            for (Gender gender : values()) {
                if (gender.value.equalsIgnoreCase(value)) {
                    return gender;
                }
            }
            return Gender.UNKNOWN;
        }
    }

    public enum ContactType {
        FB("fb"),
        PHONE("phone"),
        TWITTER("twitter"),
        UNKNOWN("unknown");

        private String value;

        ContactType(String value) {
            this.value = value;
        }

        @JsonCreator
        public static ContactType getContactTypeFromValue(final String value) {
            for (ContactType type : values()) {
                if (type.value.equalsIgnoreCase(value)) {
                    return type;
                }
            }
            return ContactType.UNKNOWN;
        }
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Name {
        private String title;
        private String first;
        private String last;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Location {
        private Street street;
        private String city;
        private String state;
        private String country;
        private String postcode;
        private Coordinates coordinates;
        private Timezone timezone;

    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Street {
        private int number;
        private String name;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    private static class Coordinates {
        private String latitude;
        private String longitude;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Timezone {
        private String offset;
        private String description;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Login {
        private String uuid;
        private String username;
        private String password;
        private String salt;
        private String md5;
        private String sha1;
        private String sha256;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class DateAndAge {
        private String date;
        private int age;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Identifier {
        private String name;
        private String value;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Picture {
        private String large;
        private String medium;
        private String thumbnail;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Client {
        private String first;
        private String last;
        private String email;
        private String address;
        private String created;
        private String balance;
        private List<Contact> contacts;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class Contact {
        private ContactType type;
        private List<ContactValue> values;
    }

    @NoArgsConstructor
    @AllArgsConstructor
    @Data
    public static class ContactValue {
        private String value;
        private Boolean isPrimary;
    }
}
