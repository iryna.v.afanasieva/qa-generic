package org.learn.qa.generic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public class TestGeneric {

  private static final ObjectMapper MAPPER = new ObjectMapper();
  private static final String EMPTY_STRING = "";

  public static void main(String[] args) {
    final var jsonString = readFile("users.json");
    System.out.println(jsonString);
    final var users = readList(jsonString, User.class);
    System.out.println(users);
  }

  private static <T> List<T> readList(final String input, final Class<T> clazz) {
    if (input == null || input.isBlank()) {
      return List.of();
    }

    try {
      JSONArray jsonArray = new JSONArray(input);
      List<T> objectList = new ArrayList<>();

      for(int index = 0; index < jsonArray.length(); index++) {
        final var object = jsonArray.getJSONObject(index);
        if (object != null) {
          objectList.add(MAPPER.readValue(object.toString(), clazz));
        }
      }

      return objectList;
    } catch (JsonProcessingException | JSONException e) {
      logAndThrow(e);
    }
    return List.of();
  }

  private static String readFile(final String fileName) {
    if (fileName == null || fileName.isBlank()) {
      return EMPTY_STRING;
    }

    try (final var jsonStream = TestGeneric.class.getClassLoader().getResourceAsStream(fileName)) {
      if (jsonStream == null) {
        return EMPTY_STRING;
      }
      return new String(jsonStream.readAllBytes(), StandardCharsets.UTF_8);
    } catch (IOException exception) {
      logAndThrow(exception);
    }
    return EMPTY_STRING;
  }

  private static void logAndThrow(final Exception exception) {
    System.out.printf("Something goes wrong : %s%n", exception.getMessage());
  }

}
